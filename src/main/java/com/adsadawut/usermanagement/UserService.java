/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


/**
 *
 * @author hanam
 */
public class UserService {
    private static  ArrayList<User> userList = new ArrayList<>();
    //Mockup
    static{
       UserService.load();
        if(!checkAdmin()){
            userList.add(new User("admin","password"));
        }
         
    }

    public static boolean checkAdmin() {
        for(User user:userList){
            if(user.getUsername().equals("admin")){
                return true;
            }
        }
        return false;
    }
    
    //Create
    public static boolean addUser(User user){
        userList.add(user);
        return true;
    }
    
    public static boolean addUser(ArrayList<User> user){
        userList.addAll(user);
        return true;
    }
    
    public static boolean addUser(String userName,String password){
        userList.add(new User(userName,password));
        return true;
    }
    
    //Update
    public static boolean updateUser(int index,User user){
        userList.set(index, user);
        return true;
    }
    
    //Read 1 user
    public static User  getUser(int index){
        if(index>userList.size()-1){
            return null;
        }
        return userList.get(index);
    }
    
    //Read all user
    public static ArrayList<User>  getUsers(){
        return userList;
    }
    
    //Search username
     public static ArrayList<User>  getUsers(String searchText){
        ArrayList<User> list = new ArrayList();
        for(User user:userList){
            if(user.getUsername().startsWith(searchText))
                list.add(user);
        }
         return list;
    }
     
     //Delete user
     public static boolean delUser(int index){
         userList.remove(index);
         return true;
     }
     
     //Delete user
     public static boolean delUser(User user){
         userList.remove(user);
         return true;
     }
     
     //Login
     public static User login(String userName,String password){
         for(User user: userList){
             if(user.getUsername().equals(userName) && user.getPassword().equals(password))
                 return user;
         }
         return null;
     }
     
     public static void save(){
         FileOutputStream fos = null;
        try {
            File file = new File("List_of_user.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(UserService.getUsers());
            oos.close();
            fos.close();
            
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
            
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                
            }
        }
     }
  
    public static void load() {
        FileInputStream fis = null;
        try {
            File file = new File("List_of_user.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            UserService.addUser((ArrayList<User>) ois.readObject());
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
            
        } catch (ClassNotFoundException ex) {
           
        } finally {
            try {
                if(fis!=null){
                    fis.close();
                }
            } catch (IOException ex) {
                
            }
        }
    }
}
